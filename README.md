# WD2N3
# Wikpedia's Dumps To Notation 3

Extract infobox from Wikipedia XML dumps and ouputs the data in RDF formatted as 
notation 3.

usage : wd2n3  ${lang}wiki-${date}-pages-articles-multistream.xml > ${lang}wiki-${date}-pages-articles-multistream.n3
