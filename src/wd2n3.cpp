#include <iostream>
#include <fstream>
#include <string>
#include <map>

using namespace std;

void FirstCharToUpper(string &s){ // Turn first character to upper case
	static map<string, string> Upper;
	map<string, string>::iterator it;
	if(Upper.find("â")==Upper.end()){
		Upper["â"]="Â";
		Upper["á"]="Á";
		Upper["à"]="À";
		Upper["ä"]="Ä";
		Upper["ê"]="Ê";
		Upper["é"]="É";
		Upper["è"]="È";
		Upper["ë"]="Ë";
		Upper["ï"]="Ï";
		Upper["í"]="Í";
		Upper["î"]="Î";
		Upper["ö"]="Ö";
		Upper["ó"]="Ó";
		Upper["ô"]="Ô";
		Upper["ö"]="Ö";
		Upper["ú"]="Ú";
		Upper["ù"]="Ù";
		Upper["û"]="Û";
		Upper["ñ"]="Ñ";
		Upper["ç"]="Ç";
	};
	for(it=Upper.begin(); it!=Upper.end();it++){
		if(s.find(it->first)==0){
			s.replace(0,it->first.length(),it->second);
			break;
		}
	}
	if(it==Upper.end())
			s[0]=toupper(s[0]);
}
// returns count of non-overlapping occurrences of 'sub' in 'str'
int countSubstring(const std::string& str, const std::string& sub)
{
    if (sub.length() == 0) 
		return 0;
    int count = 0;
    for (size_t offset = str.find(sub); offset != std::string::npos; offset = str.find(sub, offset + sub.length()))
    {
        ++count;
    }
    return count;
};

int main(int argc, char **argv){
	string line, key,alias, value;
	wstring wkey;
	unsigned int counter, count_ib=0,NumberOfValues;
	string::iterator start;
	ifstream WikiDumpFile;
	map<string, unsigned long> keys;
	unsigned long key_id=0;
	bool lastwastitle;
	
	if(argc!=2){
		cerr<<"Usage: "<<argv[0]<<" LAwiki-AAMMDD-pages-articles-multistream.xml"<<endl;
		exit(-1);
	}
	WikiDumpFile.open(argv[1]);
	if(!WikiDumpFile.good()){
		cerr << "Cannot open file '"<<argv[1]<<"' as an input"<<endl;
		exit(-1);
	}
	getline(WikiDumpFile,line);
	while(!WikiDumpFile.eof()){// Make the list of keys
		if(line.find("<title>")!=string::npos&&line.find("</title>")!=string::npos){
				key=line.substr(line.find("<title>")+7, line.length()-15-line.find("<title>"));
				if(keys.find(key)!=keys.end()){
					cerr<<"Erreur : titre '"<<key<<"' déjà enregistré"<<endl;
					exit(-1);
				};
				keys[key]=key_id++;
				if(!(key_id%10000))
					cerr<<"\r#entries : "<<key_id;
		}
		getline(WikiDumpFile,line);
	};
	cout <<endl;
	WikiDumpFile.close();
	WikiDumpFile.open(argv[1]);
	do{// outputs a notation 3 file
		getline(WikiDumpFile,line);
		lastwastitle=false;
		while(!WikiDumpFile.eof()&&line.find("{{infobox")==string::npos
						&&line.find("{{Infobox")==string::npos){
			if(line.find("<title>")!=string::npos&&line.find("</title>")!=string::npos){
				if(lastwastitle)
					cout<<" ."<<endl;
				key=line.substr(line.find("<title>")+7, line.length()-15-line.find("<title>"));
				cout<<"<#"<<keys[key]<<">\t<#wd2n3:key> \""<<key<<"\"";
				lastwastitle=true;
			}
			getline(WikiDumpFile,line);
		};
		// Now we have an infobox
		cout<<";"<<endl;
		if(WikiDumpFile.eof())
			break;
		count_ib++;
		if(!(count_ib%10000))
			cerr <<"\r#infoboxes: "<<count_ib;  
		cout<<"\t<#wd2n3:template> \"";
		if(line.find("{{infobox")!=string::npos)
			start=line.begin()+line.find("{{infobox")+9;
		else
			start=line.begin()+line.find("{{Infobox")+9;
		while(start<line.end() && (*start==' '||*start=='\t'||*start=='\n'))
			start++; // Skip spaces
		while(start<line.end() &&*start!='\t'&&*start!='\n'&&*start!='|'&&*start!='}'){
			cout << *start++;
		};
		cout << "\"";
		counter=countSubstring(line, "{{")-countSubstring(line,"}}");
		while(!WikiDumpFile.eof()&&counter!=0){// For each attribute
			//cout<<line<<endl;
			if(line.find("|")!=string::npos){// there is an attribute
				cout <<";"<<endl<<"\t<#wd2n3:";
				start=line.begin()+line.find("|")+1;
				while(start!=line.end()&&(*start==' '||*start=='\t'||*start=='\n'))
					start++;
				while(start!=line.end()&&*start!=' '&&*start!='\t'&&*start!='\n'&&*start!='='){
					cout << *start;
					start++;
				};
				cout<<">\t\t";
				if(line.find("=")==string::npos)
					break;
				start=line.begin()+line.find("=")+1;
				while(start!=line.end()&&(*start==' '||*start=='\t'))
					start++;
				value="";
				while(start!=line.end()&&*start!='\n'&&*start!='}'){
					value.push_back(*start);
					start++;
				}
				if(start!=line.end()&&*start=='}'){
					value.push_back(*start);
					start++;
				};
				if(start!=line.end()&&*start=='}'){
					value.push_back(*start);
					start++;
				}
				NumberOfValues=countSubstring(value,"[[");
				if((NumberOfValues>0)&&(NumberOfValues==countSubstring(value,"]]"))){
					bool comma=false;
					while(NumberOfValues>0){
						key=line.substr(line.find("[[")+2, line.find("]]")-line.find("[[")-2);
						if(key.find("|")!=string::npos){
							alias=key;
							key=key.substr(0, key.find("|"));
							alias=alias.substr(alias.find("|")+1, alias.length()-alias.find("|"));
							FirstCharToUpper(alias);
							if(comma)
								cout<<","<<endl<<"\t\t";
							else
								comma=true;
							if(keys.find(alias)!=keys.end())
								cout<<"<#"<<keys[alias]<<">";
							else
								cout<<"\""<<alias<<"\"";
						}
						FirstCharToUpper(key);
						if(comma)
							cout<<","<<endl<<"\t\t";
						else
							comma=true;
						if(keys.find(key)!=keys.end())
							cout<<"<#"<<keys[key]<<">";
						else
							cout<<"\""<<key<<"\"";							
						line=line.substr(line.find("]]")+2, line.length()-line.find("]]")-2);
						NumberOfValues--;
					}
				}
				else
					cout<<"\""<<value<<"\"";
			}
			getline(WikiDumpFile,line);
			counter+=countSubstring(line, "{{");
			counter-=countSubstring(line, "}}");
		};
		cout<<" ."<<endl;
	}while(!WikiDumpFile.eof());
	WikiDumpFile.close();
	return 0;
}
